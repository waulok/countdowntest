package com.obsidianloft.countdowntest;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.entity.Player;


public class Main extends JavaPlugin implements Listener {
	public final Logger logger = Logger.getLogger("Minecraft");

	public static int MinutesToCountDown=5;
	public static int SecondsToCountDown=5;
	public static Plugin plugin;
	int taskID1;
	int taskID2;
	
	@Override
	public void onEnable(){
		plugin = this;
		// Register our Listeners with the Plugin Manager
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(this, this);
	}
	
	@Override
	public void onDisable(){
		this.getServer().getScheduler().cancelAllTasks();
	}
	
	 public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	    {
	        if (commandLabel.equalsIgnoreCase("startcd"))
	        {
	            Player player = (Player) sender;
	                if(player instanceof Player){
	                    String playerName = player.getName();
	                    Bukkit.broadcastMessage(playerName + " started the game!");
	                    Bukkit.broadcastMessage(MinutesToCountDown + " minutes left until game starts!");
	                    startMinutesCountdown();
	                }
	        }
			return true;
	    }
	
	public Runnable startMinutesCountdown() {
		// REPEATING TASKS COUNTDOWN DEMO
		taskID1 = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			  public void run() {
				  MinutesToCountDown--;
				  if (MinutesToCountDown!=0) {
					  Bukkit.broadcastMessage(MinutesToCountDown + " minute(s) left until game starts!");
				  }
				  if (MinutesToCountDown==0) {
				      plugin.getServer().getScheduler().cancelTask(taskID1);
				      MinutesToCountDown=5;
				      startSecondsCountdown();
				  }

			  }
			}, 20*60L, 20*60L); // 60 seconds in a minute
		return null;
	}
	
	public Runnable startSecondsCountdown() {
		// REPEATING TASKS COUNTDOWN DEMO
		taskID2 = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			  public void run() {
				  SecondsToCountDown--;
				  if (SecondsToCountDown!=0) {
					  Bukkit.broadcastMessage(SecondsToCountDown + " second(s) left until game starts!");
				  }
				  if (SecondsToCountDown==0) {
				      plugin.getServer().getScheduler().cancelTask(taskID2);
				      SecondsToCountDown=5;
				      Bukkit.broadcastMessage("GAME STARTED!");
				  }

			  }
			}, 20L, 20L); // once each second
		return null;
	}
}
